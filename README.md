# House Prices Prediction

In this project, I'll be working with housing data for the city of Ames, Iowa, United States from 2006 to 2010. You can read more about why the data was by Dean De Cock collected [here](https://doi.org/10.1080/10691898.2011.11889627). You can also read about the different columns in the data [here](http://jse.amstat.org/v19n3/decock/DataDocumentation.txt).

**Project Goal**

The goal of this project is to implement a deep data analysis and test different ML models (multivariate) to obtain the lowest possible [RMSLE](https://medium.com/analytics-vidhya/root-mean-square-log-error-rmse-vs-rmlse-935c6cc1802a) value. I choose RMSLE value since the predictions are made to submit in the [Kaggle competition](https://www.kaggle.com/c/house-prices-advanced-regression-techniques/overview) and they use this indicator as ranking parameter.

The project is divided in 3 parts. Parts 1 and 2 create an output file (or many if different options are required) which will be used by the last part of the project as input.

## [Part 1](https://gitlab.com/ml-showcase/house-prices-prediction/-/blob/master/house_price_prediction_part1.ipynb): Numerical Features Selection

- Exploring the **train** dataset
- Managing missing values
- Correlation matrix 
- Collinearity (redundant information)
- Feature scaling (0 to 1)
- Analyze the **test** dataset and apply the process

## [Part 2](https://gitlab.com/ml-showcase/house-prices-prediction/-/blob/master/house_price_prediction_part2.ipynb): Categorical features selection

- Categorical features analysis
- Coefficient of variation
- Managing missing values
- Labeling
  - **Nominal features**: Dummy coding
  - **Ordinal features**:
    -  Count encoding
    -  Target encoding
    -  CatBoost encoding
- Feature scaling (0 to 1)

## [Part 3](https://gitlab.com/ml-showcase/house-prices-prediction/-/blob/master/house_price_prediction_part3.ipynb): Testing Feature Selection With different models

- Simple linear regression
- Grid search with cross validation
- K neighbout classifier model
- Submission file setup

**Abstract of the work related to the dataset**

" _This paper presents a data set describing the sale of individual residential property in Ames, Iowa from 2006 to 2010. The data set contains 2930 observations and a large number of explanatory variables (23 nominal, 23 ordinal, 14 discrete, and 20 continuous) involved in assessing home values. I will discuss my previous use of the Boston Housing Data Set and I will suggest methods for incorporating this new data set as a final project in an undergraduate regression course._ "

The pipeline of functions that will let us quickly iterate on different models could be represented as the following workflow:

1. **Feature selection**: _Keep only what it's useful_
    
    1. Evaluate **correlation** between features and the target. Only features that present high correlation will be kept.
    2. Evaluate then the **correlation** between different features and recognize whether there is collinearity (redundant information) or not. Those features that present collinearity will be represented by only one of them, the most representative/usefull for our analysis.
    3. Evaluate the **variaation** of the different features. Those features without enough variance are considered kind of constants and therefore will be removed

2. **Feature scaling**: _Make all features comparable_
    Each feature will be scaled from 0 to 1
3. **Feature engineering**: _Process existing features to create new ones, useful_
    1. The categorical types will not be converted to codes but instead will be converted by dummy coding. This means that for each category presented in a feature, a new feature will be generated and used as boolean. 
    2. Features with missing values will be either deleted or filled with mean values, depending in how many values are missing.