def print_as_header(s, header_width=30):
    print(header_width * "-")
    print(s)
    print(header_width * "-")