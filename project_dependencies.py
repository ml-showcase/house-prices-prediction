import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.style as style
import seaborn as sns

style.use('fivethirtyeight')
%matplotlib inline

import category_encoders as ce
import os
import re
from itertools import product

from scipy import stats
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_log_error
from sklearn import linear_model
from sklearn.model_selection import KFold

from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier